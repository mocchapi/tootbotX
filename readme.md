# NOTICE: TootbotX is no longer being developed or maintained
There will be no more feature updates or bug fixes. 

I no longer have the energy to maintain this project, especially because it kept breaking.

Ironically the most stable version of this was probably the last version of tootbotX v2, the one before the complete rewrite meant to take this to the next level :P.


## The state of things
While v3.2rc2 is feature complete as version 3.2, the code that fetches reddit posts remains janky at best. 
If you are considering spinning up anyways, i highly recommend replacing the reddit-related functions with equivalent PRAW code, which will probably actually work.

I'm not sure what the feature set is of other bots like this, but i feel like i at least substantially improved upon the original tootbot. I hope that if you're thinking about making your own bot like this, you can at least find some of the code in this repository useful.



Its been an alright ride, cheers

--------------

# [tootbotX](https://gitlab.com/mocchapi/tootbotX/) 3.2rc2
Python CLI for posting reddit posts to twitter/mastodon
    
Initially one of many [tootbot](https://github.com/corbindavenport/tootbot) forks, now sharing almost nothing but name

### Features:
**/!\\** Mastodon support is currently not implemented 
 - Automatic image caption generation with pytesseract
 - Can post to twitter and/or mastodon
 - Attaches image & video posts to twitter/mastodon directly (Including reddit galleries)
 - Vastly simplified requirements and setup
 - User-set blocklist that can filter words from titles, author names, selfposts, flairs and the image caption from pytesseract
 - Finer reddit post filters, such as minimum upvote ratio, different post types, minimum upvote count, etc
 - Ability to check multiple subreddits for posts
 - Large amount of config options to change the behavior of the bot
 - Comprehensive logging both in-terminal and in the CSV historyfile (defaulted as "./src/history.csv"), as wel as a detailed debug log saved as log.txt
 - Easy method of adding a new social network to post to in the form of a python class (see `./src/outputs.py`) 


## Installation

### Windows:
1. Install the requirements by running `py -m pip install -r requirements.txt` with cmd
2. (optional) Install [tesseract-ocr](https://github.com/tesseract-ocr/tessdoc/blob/master/Downloads.md) or [build it yourself](https://tesseract-ocr.github.io/tessdoc/Compiling.html) if you wish to use OCR for image captions and better blacklist checking
3. (optional) Install [FFMPEG](http://ffmpeg.org/download.html#get-sources) if you want the bot to post videos
4. Get platform accounts:
    - (if you want to use twitter) Apply for a [Twitter developer account](https://developer.twitter.com/en/apply/user)
    - (if you want to use mastodon) Apply for a normal user account at yout instance of choice such as [botsin.space](botsin.space) 
    - note: if you do not want to use mastodon or twitter, disable it in config.json (only twitter is enabled by default)
5. Run `tootbotX.py`
6. The tootbotX setup will ask you for some credentials from twitter and/or mastodon and take care of the rest

### Linux:
1. Install the requirements by running `python3 -m pip install -r requirements.txt` with your terminal of choice
2. (optional) Install `tesseract-ocr` with your system's package manager or [build it yourself](https://tesseract-ocr.github.io/tessdoc/Compiling.html) if you wish to use OCR for image captions and better blacklist checking
3. (optional) Install [FFMPEG](http://ffmpeg.org/download.html#get-sources) (it is likely already installed on your system) if you want the bot to post videos 
4. Get platform accounts:
    - (if you want to use twitter) Apply for a [Twitter developer account](https://developer.twitter.com/en/apply/user)
    - (if you want to use mastodon) Apply for a normal user account at yout instance of choice such as [botsin.space](botsin.space) 
    - note: if you do not want to use mastodon or twitter, disable it in config.json (only twitter is enabled by default)
5. Run `python3 tootbotx.py`
6. The tootbotX setup will ask you for some credentials from twitter and/or mastodon and take care of the rest

### Mac:
i dont have a mac i have no idea if this works at all. The linux setup might work lol

(if you figure it out, please open a pr or [dm me on twitter](https://twitter.com/Mocchapi))

## Configuration
### Changing settings
Configuration is stored in the `config.json` file as keys with a small description under each to describe it. You can reset by deleting the config.json and running the bot.

Since it is a `.json` file, your os might open it in a browser. Make sure to open it with a text editor so you can actually edit it.

### Resetting accounts
to change the twitter/mastodon account after initial setup, simply delete the`twitter_*` or `mastodon_*` files your auth folder (defaults to ./src/auth)
## Documentation
Coming someday i swear

## Accounts using tootbotX

 - [@traabot](https://twitter.com/traabot) posting from r/traaaaaaannnnnnnnnns
 - [@ennn_bot](https://twitter.com/ennn_bot) posting from r/ennnnnnnnnnnnbbbbbby

 
