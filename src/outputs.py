import tweepyauth
import os

regitems = {}

def register(class_):
    regitems[class_.name] = class_

def get(item):
    return regitems[item]

def get_registered():
    return tuple(regitems.keys())


class output():
    name = 'debug dummy'
    def __init__(self, auth_dir):
        self.auth_dir = auth_dir
    def rootify(self, path):
        return os.path.join(self.auth_dir, path)

    def post(self, post, title, redditurl, add_flair, media=None, media_alt=None):
        if add_flair:
            print(f'Dummy output "[{post.flair}] {title} {redditurl}"')
        else:
            print(f'Dummy output: "{title} {redditurl}"')
        print('Dummy media: '+','.join([f'{med}: {media_alt[idx]}' for idx, med in enumerate(media)]))
        return 'DummyPostUrl'
    
    def __str__(self):
        return f'<{self.name}>'

class TwitterMediaException(Exception):
    def __init__(self, media):
        err = media.processing_info["error"]
        out = "Twitter media error "+ err["name"] +" ("+str(err["code"]) + "): "+err["message"]
        super().__init__(out)

class twitter(output):
    name = 'twitter'
    def __init__(self, *args, api=None, **kwargs):
        super().__init__(*args, **kwargs)
        if api == None:
            self.api = tweepyauth.auto_authenticate(tokenfile=self.rootify('twitter_tokens.txt'), keyfile=self.rootify('twitter_keys.txt'))
        else:
            self.api = api
    
    def post(self, post, title, redditurl, add_flair, media=None, media_alt=None):
        if add_flair:
            text = f'[{post.flair}] {post.title}'
        else:
            text = post.title
        if len(post.title+' '+redditurl) > 280:
            text = text[:280-len(redditurl)-4]+'... '+redditurl
        else:
            text += ' ' + redditurl
        
        media_ids = []
        if media:
            media = media[:4]
            for idx, item in enumerate(media):
                media_category = "tweet_image"
                if post.contains_video:
                    media_category = "tweet_video"

                twitmedia = None
                if media_category == "tweet_video":
                    twitmedia = self.api.media_upload(item, media_category=media_category, wait_for_async_finalize=True)
                    if twitmedia.processing_info["state"] == "failed":
                        raise(TwitterMediaException(twitmedia))
                else:
                    twitmedia = self.api.media_upload(item, media_category=media_category)

                media_ids.append(twitmedia.media_id)
                x = len(media_alt)
                if x > 0 and idx <= x-1:
                    if media_alt[idx]:
                        self.api.create_media_metadata(media_ids[idx], media_alt[idx])

        tweet = self.api.update_status(text, media_ids=media_ids)    

        return f'https://twitter.com/{tweet.author.screen_name}/status/{tweet.id_str}'
    

class mastodon(output):
    name = 'mastodon'
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass



#register(mastodon)
register(twitter)
#register(output)