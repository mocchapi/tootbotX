import json
import traceback
from html import unescape

import requests

class Post():
    """Simplified and easy to access reddit post object created from a json data child. Original dict is availible through fromPosts.raw"""
    def __init__(self, raw):
        self.raw   = raw
        self.title = unescape(raw['title'])
        self.id    = raw['id']
        self.reddit_link= f'https://redd.it/{self.id}'
        self.post_hint = raw.get('post_hint')
        self.is_gallery = raw.get('is_gallery',False)
        self.is_video = raw['is_video']
        self.is_image = False
        if self.post_hint == "image":
            self.is_image = True
        # elif self.is_video and self.post_hint == "hosted:video":
        #     if raw["media"]["reddit_video"]['is_gif']:
        #         self.is_image = True
        #         self.is_video = False
        #     else:
        #         self.is_video = True

        if self.is_image:
            self.media      = [raw['url']]
            self.url        = None
        elif self.is_video:
            self.media = [raw['media']['reddit_video']['fallback_url']]
            self.url        = None
        elif self.is_gallery:
            self.media = ['https://i.redd.it/'+id_['media_id']+'.'+raw['media_metadata'][id_['media_id']]['m'].split('/')[-1] for id_ in raw.get('gallery_data',{}).get('items',[])]
            self.url = raw['url']
        else:
            self.media = None
            self.url = raw['url']
        self.flair        = raw['link_flair_text'].strip() if raw.get('link_flair_text') else None
        self.is_nsfw       = raw['over_18']
        self.is_spoiler    = raw['spoiler']
        self.is_pinned     = (raw['stickied'] or raw['pinned'])
        self.is_self = raw['is_self']
        self.text = raw.get('selftext')
        self.fullname   = raw['name']
        self.subreddit  = raw['subreddit']
        self.author     = raw['author']
        self.score      = raw['score']
        self.ratio      = raw['upvote_ratio']
        self.is_deleted = self.author == 'deleted'
        self.created_at = raw['created_utc']
        self.is_crosspost = bool(raw.get('crosspost_parent'))
        if self.is_crosspost:
            self.crosspost_parent = Post(raw['crosspost_parent_list'][0])
        else:
            self.crosspost_parent = None
        if not self.is_deleted and self.is_crosspost:
            self.is_deleted =  self.crosspost_parent.is_deleted


        self.contains_video = self.is_video or (self.is_crosspost and self.crosspost_parent.contains_video) 
        if self.contains_video and not self.is_video:
            self.media = self.crosspost_parent.media.copy()

        self.contains_image = self.is_image or self.is_gallery or (self.is_crosspost and self.crosspost_parent.contains_image)
        self.contains_media = self.contains_image or self.contains_video

    def get_media(self):
        if self.contains_media:
            if self.is_image or self.is_gallery or self.is_video:
                return self.media
            else:
                return self.crosspost_parent.get_media()
        return []

    def __str__(self):
        out = vars(self).copy()
        del out['raw']
        return '<Post, '+str(out)[1:-1]+'>'

def get_post(url, agent='Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0'):
    if not url.endswith(".json"):
        if not url.endswith('/'):
            url += '/'
        url += ".json"
    out = None
    response = requests.get(url, headers={'user-agent':agent})
    if response.text:
        try:
            posts = json.loads(response.text)[0]['data']["children"][0]["data"]
            # print(posts)
            out = Post(posts)
        except Exception as e:
            print(e)
    return out

def getRedditPosts(subreddit: str, before: str=None, after: str=None, limit: int=25, sort: str='hot', t: str=None, agent: str='Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0'):
    """Fetches reddit posts from a subreddit, returns a list of Post objects containing the relevant information.
    
    args:
        subreddit (str):          subreddit name without the r/, eg "funny"
        before    (str/None):     the fullname (see https://www.reddit.com/dev/api#fullnames) of the object you want to retreive posts posted before it
        after     (str/None):     the fullname (see https://www.reddit.com/dev/api#fullnames) of the object you want to retreive posts posted after it
        limit     (int):          maximum amount of posts to return. Actual amount may be lower. Pinned posts are always included, even if the resulting amount of posts would be higher than the limit.
        sort      (str):          what sort to use. may only be one of the following: "new", "rising", "hot", "controversial", "top"
        t         (str/None):     timeframe. may only be one of the following: "hour", "day", "week", "month", "year", "all". only applies when sort="top" or sort="controversial"
        agent     (str):          user agent to use for requests to reddit"""
    
    url = f'https://reddit.com/r/{subreddit}/{sort}/.json?limit={limit}'
    if sort in {'top', 'controversial'} and t:
        url += f'&t={t}'
    if before:
        url += f'&before={before}'
    if after:
        url += f'&after={after}'
    
    response = requests.get(url, headers={'user-agent':agent})
    out = []
    if response.text:
        posts = json.loads(response.text)['data']['children']
        out = [Post(item['data']) for item in posts]
    return out

# def filterPosts(posts, exclude=set(), images=True, NSFW=False, spoiler=False, text=False, pinned=False, self=False, video=False, deleted=False):
#     pass

class FailedFilterException(Exception):
    def __init__(self, msg):
        super().__init__(msg)

def filterPost(post, Allowlist=None, Blocklist=None, MustHaveMedia=False, ImagePostsAllowed=True, VideoPostsAllowed=False, NSFWPostsAllowed=False, SelfPostsAllowed=False, PinnedPostsAllowed=False, CrossPostsAllowed=True, SpoilerPostsAllowed=False, DeletedPostsAllowed=False, MinimumUpvoteRatio=0.75, MinimumUpvoteCount=50 ):
    """Returns nothing if the post passes all the filters & raises a FailedFilterException if it did not"""
    if not NSFWPostsAllowed and post.is_nsfw: raise FailedFilterException('Reddit post was NSFW')
    if not PinnedPostsAllowed and post.is_pinned: raise FailedFilterException('Reddit post was pinned')
    if not CrossPostsAllowed and post.is_crosspost: raise FailedFilterException('Reddit post was a crosspost')
    if not SelfPostsAllowed and post.is_self: raise FailedFilterException('Reddit post was a selfpost')
    if not SpoilerPostsAllowed and post.is_spoiler: raise FailedFilterException('Reddit post was a selfpost')
    if not ImagePostsAllowed and post.contains_image: raise FailedFilterException('Reddit post had an image')
    if not VideoPostsAllowed and post.contains_video: raise FailedFilterException('Reddit post had a video')
    if not DeletedPostsAllowed and post.is_deleted: raise FailedFilterException('Reddit post was deleted')
    if MinimumUpvoteCount > post.score: raise FailedFilterException(f'Reddit post had less than {MinimumUpvoteCount} upvotes')
    if MinimumUpvoteRatio > post.ratio: raise FailedFilterException(f'Reddit post had a smaller upvote ratio than {MinimumUpvoteRatio}')
    if MustHaveMedia and not post.contains_media: raise FailedFilterException(f"Reddit post did not contain media")
    blockAllowLists({post.title, post.author, post.flair}, Allowlist=Allowlist, Blocklist=Blocklist)

def blockAllowLists(texts, Allowlist=None, Blocklist=None):
    if Allowlist and not any([sub in [subsub for subsub in texts] for sub in Allowlist]):
        raise FailedFilterException('Post was not in allowlist')
    elif Blocklist and any([sub in [subsub for subsub in texts] for sub in Blocklist]):
        raise FailedFilterException('Post contained blocklisted terms')


def generator(subreddit, *args, backwards=False, **kwargs):
    subreddit = subreddit
    posts = getRedditPosts(subreddit, *args, **kwargs)
    if kwargs.get('after','bababooey') != 'bababooey':
        del kwargs['after']
    earliest = (99999999999, None)
    latest = (0,None)
    while True:
        for post in posts:
            if post.created_at > latest[0] and not backwards:
                latest = (post.created_at, post.fullname)
                yield post
            if post.created_at < earliest[0] and backwards:
                earliest = (post.created_at, post.fullname)
                yield post
        # print('fetching new reddit posts...')
        if backwards:
            posts = getRedditPosts(subreddit, *args, before=earliest[1], **kwargs)
        else:
            posts = getRedditPosts(subreddit, *args, after=latest[1], **kwargs)
        if posts == None:
            posts = []
            yield None

def bigenerator(subreddit, *args, start_forwarded=True, **kwargs):
    forwards = generator(subreddit, *args, backwards=False, **kwargs)
    backwards = generator(subreddit, *args, backwards=True, **kwargs)
    if start_forwarded:
        current = forwards
    else:
        current = backwards
    for post in current:
        if post == None:
            if current is forwards:
                current = backwards
            else:
                current = forwards
        else:
            yield post

def simplegenerator(subreddit, *args, history=[], minimum_pool=5, max_tries=3, logger=None, **kwargs):
    """Due to the extremely bad success rate of the original generator, this one has been made as a temporary replacement.
    It's logic is very similar to the TBX2.0 post fetcher, which does work decently.
    A queue is kept, with minimum_pool indicating it's minimum size. If the size of the queue falls below the minimum, new posts will be collected.
    If this tries and fails to get new posts `max_tries` times, it will use up one of the queue posts.
    This is to reduce the amount of times a post timeframe gets skipped due to not finding enough new posts.
    """
    def talk(*args):
        if logger != None:
            logger.info(*args)
    def warn(*args):
        if logger != None:
            logger.warning(*args)
    def yell(*args):
        if logger != None:
            logger.error(*args)
    def nerd(*args):
        if logger != None:
            logger.debug(*args)
    history = set(history)
    queue = []
    tries = 0
    while True:
        if tries >= max_tries:
            warn(f"Maximum tries exceeded! [{tries}/{max_tries}]")
            tries = 0
            if len(queue) > 0:
                out = queue[0]
                queue.pop(0)
                yield out
            else:
                yield None

        for i in range(max(len(queue)-minimum_pool,0)):
            post = queue[0]
            # if not post.fullname in history:
            history.add(post.fullname)
            queue.pop(0)
            yield post

        if tries == 0:
            talk("Fetching new posts...")
        
        try:
            queue += getRedditPosts(subreddit, *args, before=None, after=None, **kwargs)
        except Exception as e:
            yell("An error occured: "+str(e))
            nerd(traceback.format_exc())

        # filter out the ones that have already passed
        if len(queue) > 0:
            queue = [q for q in queue if not q.fullname in history]

        if len(queue) == 0:
            talk("...")
            tries += 1


def test():
    print(get_post("https://www.reddit.com/r/CatsWhoChirp/comments/s0ml2t/i_was_thinking_the_same_thing/.json"))
    print(get_post("https://www.reddit.com/r/dankvideos/comments/sikl7k/im_only_business/.json"))


if __name__ == '__main__':
    test()