#!/usr/bin/python3
import os
import sys
import csv
import math
import time
import shutil
import random
import logging
import datetime
import argparse
import traceback
import subprocess

import json5
import requests
import pytesseract
from PIL import Image, ImageEnhance, ImageOps

import outputs
import redditApi

LOG = logging.getLogger('tootbotX')
LOG.setLevel(logging.DEBUG)

__VERSION__ = 'v3.2rc2'

ROOT = os.path.dirname(os.path.realpath(__file__))
DEFAULT_CONFIG = '{\n   "EnabledOutputs": ["twitter"],\n   // List of all outputs to have the bot post to\n   // The default availible options are "twitter" and "mastodon"\n   // You can select multiple outputs like this: ["twitter", "mastdodom"] note that only one instance of each output can be used\n   // If you are using a plugin output, check it\'s documentation for its output strings. If running in verbose mode, the bot will print a list of availible outputs.\n\n   "Subreddits": ["ennnnnnnnnnnnbbbbbby"],\n   // Name of subreddit(s) to take posts from (example: ["traaaaaaannnnnnnnnns"])\n   // Multiple subreddits can be used like this: ["traaaaaaannnnnnnnnns","Opossum","peepy"]\n   // Note that the subreddit names shouldn\'t start with "r/"\n\n   "TempFolder": "tmp",\n   // Directory to store temporary files in, such as images (default is "tmp")\n\n   "AuthFolder": "auth",\n   // Directory to store login details for outputs in, such as twitter tokens (default is "auth")\n\n   "HistoryFile": "history.csv",\n   // spreadsheet which stores all information about posts made by the bot (default is history.csv)\n\n   "KeepFilesRooted": true,\n   // Ensure that the history file. tempfolder & authfolder paths are relative to the script\'s location, and not the current terminal\'s\n\n   "PostDelay": 15,\n   // Amount of minutes between bot posts (default is 15)\n\n   "DelayConsistency": true,\n   // if set to true, will check latest history log on startup and calculate how much time should be slept so the time between posts stays consistent (default is false)\n\n   "Sort": "hot",\n   // Subreddit sort to use. Must be one of the following: "new", "rising", "hot", "controversial", "top" (default is "hot")\n\n   "SubRandomisation": true,\n   // If set to true & more than one subreddit is selected in Subreddits, the bot will randomly pick from which sub to post something\n   // If set to false & more than one subreddit is selected in Subreddits, the bot will go down the list in the same order (default is true)\n\n   "AddFlairs": true,\n   // Adds the reddit post flair to the bot\'s outputs, eg "[myflair] look at this cool post!! http://redd.it/someid" (default is true)\n\n   "OCR": {\n      "OcrEnabled": true,\n      // Use optical character regocnition to generate alt-text from images for accessibility. (default is true) \n      // Note that Tesseract-ocr, Pillow and pytesseract must be installed, see https://github.com/tesseract-ocr/tesseract, https://pypi.org/project/Pillow/ and https://pypi.org/project/pytesseract/ \n   \n      "OcrIgnoreEffectExceptions": true,\n      // Ignore errors on ocr effects such as contrast ans posterize\n\n      "OcrMinConf": 85,\n      // Minimum percentage of confidence in a word to use it (default is 85)\n\n      "OcrAllowChars": "!?$-=+ \\"\\\'.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/()",\n      // String of allowed characters for tesseract ocr. Leave be unless your know what you\'re doing. (default is "!?$-=+ \\"\\\'.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/()")\n      \n      "OcrLang": "eng",\n      // Language to use with tesseract (default is "eng")\n\n      "OcrContrastChange": 1.5,\n      // Amount of contrast to add / subtract to the image before passing it along to tesseract. Set to 1.0 to disable (default is 1.5)\n\n      "OcrPosterizeBits": 1,\n      // Posterization of the image before getting passed along to tesseract. Set to 0 to disable (default is )\n\n      "OcrGrayscale": true,\n      // If set to true, the image media will be converted to grayscale before getting passed along to tesseract (default is true)\n   },\n\n   "Filters": {\n      "MustHaveMedia": true,\n      // If enabled, it filters out any post that is not a video, image, or gallery.\n      // The filters for videos and images still apply, so make sure at least one of them is set to true\n      // This ensures that no reddit links get posted and all media is uploaded directly to the social media\n      // (default is true0)\n\n      "MinimumUpvoteRatio": 0.75,\n      // Minimum upvote:downvote ratio a reddit post must have to get posted by the bot (default is 0.75)\n\n      "MinimumUpvoteCount": 50,\n      // Minimum amount of upvotes a reddit post must have to get posted by the bot (default is 50)\n\n      "ImagePostsAllowed": true,\n      // Allow Reddit posts with images to be posted by the bot (default is true)\n\n      "VideoPostsAllowed": true,\n      // Allow Reddit posts with videos to be posted by the bot (default is true)\n\n      "NSFWPostsAllowed": false,\n      // Allow NSFW Reddit posts to be posted by the bot (default is false)\n\n      "SpoilerPostsAllowed": false,\n      // Allow Reddit posts marked as spoilers to be posted by the bot (default is false)\n\n      "SelfPostsAllowed": false,\n      // Allow Reddit self-posts (text posts) to be posted by the bot (default is false)\n\n      "DeletedPostsAllowed": false,\n      // Allow reddit posts that have since been deleted by moderators to be posted by the bot (default is false)  \n      // Note that the images from deleted posts are often the "If you are looking for an image, it was probably deleted." image \n\n      "PinnedPostsAllowed": false,\n      // Allow posts pinned by the moderators of the subreddit to be posted by the bot (default is false)\n\n      "CrossPostsAllowed": true,\n      // Allow reddit x-posts to be posted by the bot (default is true)\n      // The bot post will link to the crosspost, but if the parent has any media it will upload that too (if allowed by the filters)\n\n      "Blocklist": [],\n      // List of banned words. Any post that contains any of these words will get skipped. Leave empty to disable (default is [])\n      // Words are seperated by comma\'s and must be surrounded by qoutation marks ["like", "this", "and this"]\n\n      "Allowlist": [],\n      // List of words that a reddit post must contain in order to be posted. Takes priority over the blocklist, leave empty to disable (default is [])\n      // Words are seperated by comma\'s and must be surrounded by qoutation marks ["like", "this", "and this"]\n\n   },\n\n   "AllowTranscoding": true,\n   // Video\'s will automatically get retranscoded to include audio, alongside fixing reddit videos to work with twitter.\n   // If this is disabled, many videos will not have any sound.\n   // See https://twittercommunity.com/t/video-track-is-not-present-uploading-mp4/103570/3 for more info\n   // (default is true) \n\n   "TranscodeArguments": [],\n   // Additional FFMPEG arguments. Only used when AllowTranscoding is set to true.\n   // Best to not touch this if you don\'t know what this means. (default is [])\n\n   "MinPostPool": 10,\n   // The target amount of posts that should be kept in-queue. (default is 10)\n   // The queue is a backlog of reddit posts,\n   // tootbotX tries to always have at least this many posts on stand-by.\n   // This is useful for when no new reddit posts can be found, and when the maximum fetch retry count is exceeded,\n   // one of the queue posts will be used instead.\n\n   "MaxFetchTries": 3,\n   // Maximum amount of retying for new posts when none are found. (default is 3)\n   // Increasing this number has little effect,\n   // as querying reddit multiple times in quick succession will mostly return the same posts\n\n   "FetchLimit": 25,\n   // Amount of posts to fetch at once. Max is 100 (default is 25)\n\n   "KeepTempFiles": false,\n   // Prevents deletion of temporary files, such as images after a post is made. (default is false)\n\n}'

class IncorrectFileFormatException(Exception):
    def __init__(self, msg):
        super().__init__(msg)

def clear_dir_files(dir_):
    for item in os.listdir(dir_):
        if os.path.isfile(os.path.join(dir_, item)):
            fullpath = os.path.join(dir_, item)
            os.remove(fullpath)
            LOG.debug(f'Deleted {fullpath}')


def transcode_video(path, newpath, audiopath=None, arguments=[]):
    front = ["ffmpeg","-i",path]
    back = ["-f","mp4","-nostdin","-y",newpath]
    if audiopath != None:
        front += ["-i", audiopath, "-c:v", "copy", "-c:a", "aac"]
    return subprocess.run(front + arguments + back).returncode == 0


def download(url, path, file_formats=('png','jpg','gif','jpeg','mp4','webm'), transcode=True, transcode_args=[], ignore_format=False, fallback_extension="mp4"):
    """download a file, return the path to the downloaded file"""
    LOG.debug(f'Target url: {url}')
    LOG.debug("Downloading to: "+path)
    filename = url.split('/')[-1]
    fuller_filename = filename.split('.')[0].replace('.','_').replace("?","_").replace("=","_")
    r = requests.get(url)
    mime = r.headers.get('content-type')
    extension = mime.split("/")[-1]

    if ignore_format or extension in file_formats:
        if ignore_format:
            extension = fallback_extension
        real_filename = fuller_filename+"."+extension
        filenamePath = os.path.join(path, real_filename)
        with open(filenamePath, 'wb') as f:
            f.write(r.content)
        if mime.lower().startswith("video") and transcode:
            LOG.info("Starting transcode")
            audio_url = "/".join(url.split('/')[0:-1]) + "/DASH_audio.mp4"
            
            LOG.info("Downloading audio...")
            audio_file = None
            try:
                audio_file = download(audio_url, os.path.join(path, path), transcode=False, file_formats=["mp4"])
            except IncorrectFileFormatException as e:
                LOG.error("Downloading audio track failed: "+str(e))
                LOG.debug(traceback.format_exc())

            LOG.info("Transcoding "+filenamePath+"...")
            transpath = os.path.join(path, fuller_filename+"_transcoded."+extension)
            if transcode_video(filenamePath, transpath, audiopath=audio_file, arguments=transcode_args):
                LOG.info("Done.")
                return transpath
            LOG.error("An error occured while transcoding. Check your FFMPEG installation.")
        return filenamePath
        # if format_ not in file_formats or guess_extension:
        #     kind = filetype.guess(filenamePath)
        #     if kind != None:
        #         ext = kind.extension
        #         LOG.debug(f'Guessed file {filenamePath} as "{kind.extension}')
        #     else:
        #         LOG.warning(f'Couldnt guess filetype for {filenamePath}, falling back on "{fallback_extension}"')
        #         ext = fallback_extension
        #     filenamePathNew = os.path.join(path, )+"."+ext.lstrip("."))
        #     os.rename(filenamePath, filenamePathNew)
        #     filenamePath = filenamePathNew
                
    else:
        raise IncorrectFileFormatException(f'Unsupported file format for url "{url}": "{format_}". Must be one of the following: ({", ".join(file_formats)})')

def get_ocr(image, ignoreeffectexceptions=True, posterize=1,grayscale=True, contrast=1.5, min_conf=85, allowlist="!?$-=+\\\"\\\'.0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/()", lang='eng'):
    img = Image.open(image)
    try:
        if contrast != 1.0:
            img = ImageEnhance.Contrast(img).enhance(contrast)
        if grayscale:
            img = ImageOps.grayscale(img)
        if posterize > 0:
            img = ImageOps.posterize(img, posterize)
    except Exception as e:
        if ignoreeffectexceptions:
            LOG.warning(f'Ignored OCR effect error: {e}')
            LOG.debug(traceback.format_exc())
        else:
            raise e

    ocr = pytesseract.image_to_data(img, lang=lang, config=f'-c tessedit_char_whitelist="{allowlist}"', output_type=pytesseract.Output.DICT)
    text = ' '.join([word for idx, word in enumerate(ocr['text']) if int(ocr['conf'][idx]) > min_conf and (word != ' ' or word != '')])
    if text == '': return None
    text += '.' if not text.endswith('.') else ''
    return text

def notify_sleep(sleeptime, interval=5*60, reason=""):
    """Sleeps for `sleeptime` amount of time, notifying the user every `interval` seconds.
    Optionally supplied with a `reason`, which gets appended to the first message"""
    timechunk = sleeptime/interval
    timeleft = sleeptime
    LOG.info(f"Sleeping for {round(sleeptime/60,1)}m"+reason)
    loop2 = False
    while math.floor(timeleft) > 0:
        zzzz = 0
        if sleeptime > interval:
            zzzz = interval
        else:
            zzzz = timeleft
            
        if loop2:
            LOG.info(f"{round(timeleft/60,1)}m remaining.")
        else:
            loop2 = True
        timeleft -= zzzz
        time.sleep(zzzz)

class TootbotX():
    def __init__(self, outputs_, subreddits, postdelay=15, consistent=True, filters={}, subrandomisation=True, transcode=True, transcodeargs=[], generatorargs={}, historyfile='history.csv', tempfolder='tmp', ocrconfig={}, addflairs=True, redditsort='hot', fetchlimit=25, keeptempfiles=False ):
        self.outputs = outputs_
        self.filters = filters
        self.postdelay = postdelay
        self.historyfile = historyfile
        self.consistent = consistent
        self.tempfolder = tempfolder
        self.subrandomisation = subrandomisation
        self.OcrConfig = ocrconfig
        self.subidx = 0
        self.addflairs = addflairs
        self.keeptempfiles = keeptempfiles
        self.transcode = transcode
        self.transcodeargs = transcodeargs
        self.generatorargs = generatorargs.copy()
        if self.transcode:
            if shutil.which("ffmpeg") == None:
                LOG.error("FFMPEG is not installed, but transcoding has been enabled in config!")
                LOG.error("Please install FFMPEG fom https://ffmpeg.org/ and add it to your path, or disable transcoding.")
                LOG.warn("continuing without transcoding.")
                self.transcode = False



        LOG.info("Reading history...")
        with open(self.historyfile,'r', newline='') as f:
            self.db = list(csv.DictReader(f))
        # can possibly be kept local to this function, as it's only used to hand off the fullname list to the generators
        # fullnames look like IDs, i dont know why its like that but they seem to be unique identifiers so whatever

        # latest = None
        # if len(self.db) > 0:
        # self.fullname_blocklist = set([item['reddit fullname'] for item in self.db])
        # LOG.debug(f'Excludes list: {self.fullname_blocklist}')
        LOG.info("Loading generators...")
        history = [item['reddit fullname'] for item in self.db][-500:]
        self.subreddits = [redditApi.simplegenerator(sub, history=history, logger=LOG, sort=redditsort, **self.generatorargs) for sub in subreddits]
        LOG.info("**Ready.**")

    def get_latest_success(self):
        for item in reversed(self.db):
            if item['result'] in {'output success', 'partial output faillure'}:
                return item
                

    def run(self):
        if len(self.db) > 0 and self.consistent:
            previous = float(self.db[-1]['timestamp'])
            deltatime = (time.time() - previous)
            new = deltatime % (self.postdelay * 60)
            sleeptime = (self.postdelay*60) - new
            notify_sleep(sleeptime, reason=" to keep posting consistent.")
            # LOG.info(f'Sleeping for {round(sleeptime/60,1)}m to keep posting consistent')
            # time.sleep(sleeptime/3)
            # for i in range(2,0,-1):
            #     LOG.info(f'{round(sleeptime/3*i/60,1)}m remaining')
            #     time.sleep(sleeptime/3)
        while True:
            self.step()
            notify_sleep(self.postdelay*60, reason=" until the next post, as configured.")
            # LOG.info(f'Sleeping for {self.postdelay}m')
            # time.sleep((self.postdelay/3)*60)
            # for i in range(2,0,-1):
            #     LOG.info(f'{round(self.postdelay/3*i,1)}m remaining')
            #     time.sleep((self.postdelay/3)*60)

    def make_entry(self, post, result_type, result):
        '"date","subreddit","reddit url","reddit fullname","result type","result"'
        entry = {'timestamp':time.time(), 'date':datetime.datetime.now(), 'subreddit':post.subreddit, 'reddit url':post.reddit_link, 'reddit fullname':post.fullname, 'title':post.title, 'result type':result_type, 'result':result}
        self.db.append(entry)
        # self.fullname_blocklist.add(post.fullname)
        LOG.debug("Made entry: "+str(entry))
        with open(self.historyfile, 'a') as f:
            a = csv.DictWriter(f, fieldnames=entry.keys())
            a.writerow(entry)

    def step(self):
        if self.subrandomisation:
            sub = random.choice(self.subreddits)
        else:
            sub = self.subreddits[self.subidx]
            self.subidx = (self.subidx + 1) % (len(self.subreddits) - 1)
        try:
            for post in sub:
                try:
                    if post == None:
                        LOG.warn("Couldn't fetch any new posts.")
                        return
                    redditApi.filterPost(post, **self.filters)
                    # if post.fullname in self.fullname_blocklist:
                    #     LOG.info(f'Skipping post {post.reddit_link}: Post already in historyfile')
                    #     continue
                    break
                except redditApi.FailedFilterException as e:
                    LOG.info(f'Skipping post {post.reddit_link}: {e}')
                    self.make_entry(post, 'skipped', e)
                except Exception as e:
                    LOG.error(f'An unexpected error occured, skipping post {post.reddit_link} ({e})')
                    LOG.debug(traceback.format_exc())
                    self.make_entry(post, 'unknown error', traceback.format_exc())
            LOG.info(f'Preparing post {post.reddit_link} "{post.title}"')
            LOG.debug(f'Post: {post}')
            # LOG.debug(f'raw: {post.raw}')
            fails = 0
            files = []
            alt_texts = []
            try:
                if post.contains_media:
                    medias = post.get_media()
                    LOG.info(f'Downloading {len(medias)} media')
                    for media in medias:
                        mediapath = download(media, self.tempfolder, fallback_extension='.mp4', transcode=self.transcode, transcode_args=self.transcodeargs)
                        files.append(mediapath)
                        LOG.info(f"  Downloaded {media} as {mediapath}")
                    LOG.info('All media downloaded')
                    if self.OcrConfig.get('OcrEnabled') and (post.contains_image):
                        LOG.info('Processing OCR')
                        for idx, file in enumerate(files):
                            ocr_out = get_ocr(file, posterize=self.OcrConfig['OcrPosterizeBits'], min_conf=self.OcrConfig['OcrMinConf'], grayscale=self.OcrConfig['OcrGrayscale'], allowlist=self.OcrConfig['OcrAllowChars'], lang=self.OcrConfig['OcrLang'], contrast=self.OcrConfig['OcrContrastChange'])
                            alt_texts.append(ocr_out)
                            if ocr_out:
                                redditApi.blockAllowLists([ocr_out], self.filters['Allowlist'], self.filters['Blocklist'])
                                LOG.info(f'  OCR file #{idx} output: {ocr_out}')
                            else:
                                LOG.warning(f'  Couldnt process OCR for file #{idx}')
                results = []
                LOG.info('Posting to outputs.')
                for output in self.outputs:
                    try:
                        LOG.info(f'  Posting to {output.name}')
                        o = output.post(post, post.title, post.reddit_link, self.addflairs if post.flair != None else False, media=files, media_alt=alt_texts)
                        LOG.info(f'  Post sucessful: "{o}"')
                        results.append(output.name+': '+o)
                    except Exception as e:
                        LOG.error(f'Error while posting to {output.name}: {e}')
                        trace = traceback.format_exc()
                        LOG.debug(trace)
                        fails += 1
                        results.append(f'{output.name}: "{trace}"')
                
                if fails == 0: result_type = 'output success'
                elif fails < len(self.outputs): result_type = 'partial output faillure'
                elif fails >= len(self.outputs): result_type = 'complete output faillure'
                LOG.debug('Result type: '+result_type)
                self.make_entry(post, result_type, '\n'.join(results))
                if not self.keeptempfiles:
                    LOG.info('cleaning up...')
                    clear_dir_files(self.tempfolder)
                else:
                    LOG.info("Skipped cleaning up.")
                LOG.info('All done')

            except IncorrectFileFormatException as e:
                LOG.error(f'Error while downloading file: {e}')
                self.make_entry(post, 'media error', e)
            except redditApi.FailedFilterException as e:
                LOG.info('OCR has found allowlist/blocklist breaking terms in the image, skipping...')
                self.make_entry(post, 'skipped by OCR', e)
            except Exception as e:
                LOG.error(f'An unexpected error occured, skipping post {post.reddit_link}')
                LOG.debug(traceback.format_exc())
                self.make_entry(post, 'uknown error', traceback.format_exc())
        except Exception as e:
            LOG.error(e)
            LOG.debug(traceback.format_exc())
            self.make_entry(post, 'uknown error', traceback.format_exc())


            

def load_config(config_path=os.path.join(ROOT,"config.json")):
    """Feel like this is as self explenatory as it can get"""
    LOG.info('Loading config')
    # config_path = os.path.join(ROOT, 'config.json')
    LOG.debug(f'Config path: {config_path}')
    config = json5.loads(DEFAULT_CONFIG)
    if os.path.isfile(config_path):
        try:
            with open(config_path,'r') as f:
                usr_config = json5.loads(f.read())
                config.update(usr_config)
                LOG.info('Config loaded')
        except Exception as e:
            LOG.error(f'Cant load config: "{e}"')
            LOG.warning(f'Falling back on default config')
    else:
        LOG.warning(f'No such file: {config_path}')
        LOG.warning(f'Falling back on default config & creating new config file')
        with open(config_path,'w') as f:
            f.write(DEFAULT_CONFIG)
    if config['KeepFilesRooted']:
        config['HistoryFile'] = os.path.join(ROOT, config['HistoryFile'])
        config['TempFolder'] = os.path.join(ROOT, config['TempFolder'])
        config['AuthFolder'] = os.path.join(ROOT, config['AuthFolder'])
    LOG.debug(f'Config: {config}')
    return config


def get_args(args):

    parser = argparse.ArgumentParser(description='Reddit crossposter bot for twitter/mastodon')

    parser.add_argument('--verbose', help='Use verbose logging', action='store_true')
    parser.add_argument('--no-consistency', help='Skip waiting until the next consistent time to post, even if set in the configuration.', action='store_true')
    parser.add_argument('--no-logfile', help='Disable logging to logfile', action='store_true')
    parser.add_argument('--overwrite-logfile', help='Delete the old logfile and replace it with the logs for this run', action='store_true')
    parser.add_argument('--logfile', help='File to output logs to', default=os.path.join(ROOT, 'log.txt'))
    parser.add_argument('--config', help='File to use as logfile', default=os.path.join(ROOT, 'config.json'))
    parser.add_argument('--reset-auth', help='Delete all stored output authentication', action='store_true')
    parser.add_argument('--reset-config', help='Reset the config file to it\'s default parameters', action='store_true')
    parser.add_argument('--version', help='Print version and exit', action='store_true')

    return vars(parser.parse_args(args))


def main(args_: list):
    """Hanldes setup and confrims that stuff exists"""
    args = get_args(args_)
    if args['version']:
        print(__VERSION__)
        return
    
    debugformatter = logging.Formatter('%(asctime)s [ %(module)s :: %(funcName)-20s :: %(lineno)5s ] %(levelname)8s -> %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    infoformatter = logging.Formatter('[%(asctime)s] %(levelname)8s: %(message)s', datefmt='%H:%M:%S')

    texthandler = logging.StreamHandler(sys.stdout)

    texthandler.setFormatter(infoformatter)
    if args['verbose']:
        texthandler.setLevel(logging.DEBUG)
    else:
        texthandler.setLevel(logging.INFO)
    LOG.addHandler(texthandler)

    if not args['no_logfile']:
        if args['overwrite_logfile']:
            if os.path.isfile(args['logfile']):
                os.remove(args['logfile'])
                LOG.info('Old logfile cleared')
            else:
                LOG.warning('Logfile does not exist, cannot be cleared')
        filehandler = logging.FileHandler(args['logfile'])
        filehandler.setFormatter(debugformatter)
        LOG.addHandler(filehandler)

    if not args['overwrite_logfile']: LOG.debug('\n\n')
    LOG.debug('#'*50)
    LOG.debug('#'+f'tootbotX {__VERSION__}'.center(48)+'#')
    LOG.debug('#'+f'{datetime.datetime.now()}'.center(48)+'#')
    LOG.debug('#'+f'https://gitlab.com/mocchapi/tootbotX'.center(48)+'#')
    LOG.debug('#'+f'This software is licensed under GPLv3'.center(48)+'#')
    LOG.debug('#'*50)
    LOG.debug('')

    LOG.debug(f'Arguments: {args}')

    if args['reset_config']:
        try:
            os.remove(args['config'])
        except Exception as e:
            LOG.error(f'Couldn\'t reset config: "{e}"')
            LOG.debug(traceback.format_exc())

    config = load_config()

    if args["no_consistency"]:
        LOG.debug("Skipping consistency timing because --no-consistency was passed.")
        config["DelayConsistency"] = False

    if args['reset_auth']:
        try:
            clear_dir_files(config['AuthFolder'])
        except Exception as e:
            LOG.error(f'Couldn\'t reset authentications: "{e}"')
            LOG.debug(traceback.format_exc())

    for directory in {config['TempFolder'], config['AuthFolder']}:
        if not os.path.isdir(directory):
            try:
                LOG.warn(f'No such directory: "{directory}", creating one now.')
                os.mkdir(directory)
            except Exception as e:
                LOG.critical(f'Couldnt create directory "{directory}": {e}')
                return

    if not os.path.isfile(config['HistoryFile']):
    # make sure the historyfile exists and if not, make one with the correst headers
        try:
            LOG.warning(f'HistoryFile "{config["HistoryFile"]}" doesnt exist, creating one now.')
            with open(config['HistoryFile'],'w') as f:
                # f.write('"date","subreddit","reddit url","reddit fullname","result type","result"')
                csv.DictWriter(f, fieldnames=["timestamp","date","subreddit","reddit url","reddit fullname","title","result type","result"]).writeheader()
        except Exception as e:
            LOG.critical(f'Couldnt create file "{directory}": {e}')
            LOG.debug(traceback.format_exc())
            return
    LOG.debug(f'Available outputs: {outputs.get_registered()}')

    if config['EnabledOutputs'] == []:
        LOG.critical(f'No outputs listed in config, please add one of the following to youre EnabledOutputs list: {outputs.get_registered()}')
        return
    
    LOG.info('Loading outputs')
    loaded_outputs = []
    for output in config['EnabledOutputs']:
        LOG.info(f'  Loading output "{output}"')
        try:
            new = outputs.get(output)
            loaded_outputs.append(new(config['AuthFolder']))
            # LOG.info(f'Output "{output}" loaded')
        except Exception as e:
            LOG.error(f'  Couldnt load output "{output}": "{e}"')
            LOG.debug(traceback.format_exc())
        except KeyboardInterrupt:
            print()
            LOG.warninging('Loading cancelled by user')
    if loaded_outputs == []:
        LOG.critical('No outputs loaded correctly, please review the logged errors and adjust acordingly')
        return
    LOG.info(f'{len(loaded_outputs)}/{len(config["EnabledOutputs"])} outputs loaded')
    bot = TootbotX(loaded_outputs, config['Subreddits'], consistent=config['DelayConsistency'], transcodeargs=config["TranscodeArguments"], filters=config['Filters'], postdelay=config['PostDelay'], transcode=config["AllowTranscoding"], generatorargs={"limit":config["FetchLimit"], "minimum_pool":config["MinPostPool"], "max_tries":config["MaxFetchTries"]}, historyfile=config['HistoryFile'], tempfolder=config['TempFolder'], ocrconfig=config['OCR'], subrandomisation=config["SubRandomisation"], addflairs=config["AddFlairs"], redditsort=config["Sort"], fetchlimit=config["FetchLimit"], keeptempfiles=config["KeepTempFiles"])
    try:
        bot.run()
    except KeyboardInterrupt:
        print()
        LOG.info('User issued keyboard interupt, exiting')
        if not config["KeepTempFiles"]:
            clear_dir_files(config['TempFolder'])
        LOG.info('')
        LOG.debug("Context: "+traceback.format_exc())
        LOG.info('Goodbye')
    except Exception as e:
        LOG.critical(traceback.format_exc())


if __name__ == '__main__':
    main(sys.argv[1:])